import { Component } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent {
 name : string ="Deepak";
 marks : number = 20;
 n1  = 90;
 n2 = 98;
 value1 : string ="Its not enabled";
 flag:boolean = true;
 value2 : string ="Its enabled";

  msg="2 way data binding";
  Change()
  {
     this.msg=" Value is changed";
  }
   Hello()  : void
 {
   console.log("Hello");
  }
  
 }


