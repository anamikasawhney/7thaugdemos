import { Component } from '@angular/core';

@Component({
  selector: 'demo-fourth',
  templateUrl: './fourth.component.html',
  styleUrls: ['./fourth.component.css']
})
export class FourthComponent {
myStyle = 
{ 
'color':'green',
'font-size':'small'}
flag : boolean = true;
style1: string ="105px";

multiClasses=
{
 box1: true,
 box2: false,
 box3: true
}
// box1 =
// { 'background-color':'gray',
//   'color':'red',
//   'border': '1px solid yellow'

// }
}