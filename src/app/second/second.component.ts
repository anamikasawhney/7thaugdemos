import { Component } from '@angular/core';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent {
  //  courses : any =[];
  courses = 
   [{name:"C#", duration: 20},
   {name:"Java", duration: 70},
  {name:"Php", duration: 20}];

  AddCourse()
  {
     this.courses.push({name:"new course", duration:90});
  }
  
}


